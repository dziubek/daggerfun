package co.untitledkingdom.daggerfun.injection

import android.content.Context
import co.untitledkingdom.daggerfun.FunApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(funApplication: FunApplication): Context {
        return funApplication
    }
}