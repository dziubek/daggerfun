package co.untitledkingdom.daggerfun.injection.settings

import co.untitledkingdom.daggerfun.injection.scopes.FragmentScope
import co.untitledkingdom.daggerfun.injection.settings.item.ItemModule
import co.untitledkingdom.daggerfun.settings.item.ItemFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBinder {

    @FragmentScope
    @ContributesAndroidInjector(modules = [ItemModule::class])
    abstract fun bindItemFragment(): ItemFragment
}