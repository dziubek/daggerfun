package co.untitledkingdom.daggerfun.injection.scopes

import javax.inject.Scope

@Scope
annotation class FragmentScope