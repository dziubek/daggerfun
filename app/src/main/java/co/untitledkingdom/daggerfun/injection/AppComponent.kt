package co.untitledkingdom.daggerfun.injection

import co.untitledkingdom.daggerfun.FunApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    AndroidInjectionModule::class,
    ActivityBinder::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(funApplication: FunApplication): Builder

        fun build(): AppComponent
    }

    fun inject(funApplication: FunApplication)
}