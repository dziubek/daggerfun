package co.untitledkingdom.daggerfun.injection

import co.untitledkingdom.daggerfun.detail.DetailActivity
import co.untitledkingdom.daggerfun.injection.detail.DetailModule
import co.untitledkingdom.daggerfun.injection.main.MainModule
import co.untitledkingdom.daggerfun.injection.scopes.ActivityScope
import co.untitledkingdom.daggerfun.injection.settings.FragmentBinder
import co.untitledkingdom.daggerfun.injection.settings.SettingsModule
import co.untitledkingdom.daggerfun.main.MainActivity
import co.untitledkingdom.daggerfun.settings.SettingsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBinder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [DetailModule::class])
    abstract fun bindDetailActivity(): DetailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SettingsModule::class, FragmentBinder::class])
    abstract fun bindSettingsActivity(): SettingsActivity
}