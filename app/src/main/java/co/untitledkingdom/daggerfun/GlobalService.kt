package co.untitledkingdom.daggerfun

import android.content.Context
import javax.inject.Inject

class GlobalService @Inject constructor(private val context: Context)