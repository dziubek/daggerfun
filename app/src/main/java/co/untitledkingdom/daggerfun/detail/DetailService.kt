package co.untitledkingdom.daggerfun.detail

import co.untitledkingdom.daggerfun.GlobalService
import javax.inject.Inject

class DetailService @Inject constructor(private val globalService: GlobalService)