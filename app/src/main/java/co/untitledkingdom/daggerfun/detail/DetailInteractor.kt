package co.untitledkingdom.daggerfun.detail

import javax.inject.Inject

class DetailInteractor @Inject constructor(private val detailService: DetailService)