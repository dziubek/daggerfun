package co.untitledkingdom.daggerfun.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import co.untitledkingdom.daggerfun.FunApplication
import co.untitledkingdom.daggerfun.R
import co.untitledkingdom.daggerfun.injection.detail.DetailModule
import dagger.android.AndroidInjection
import javax.inject.Inject

class DetailActivity : AppCompatActivity() {

    @Inject
    lateinit var detailPresenter: DetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        detailPresenter.bind()
    }
}