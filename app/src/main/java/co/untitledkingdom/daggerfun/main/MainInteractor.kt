package co.untitledkingdom.daggerfun.main

import javax.inject.Inject

class MainInteractor @Inject constructor(private val mainService: MainService)