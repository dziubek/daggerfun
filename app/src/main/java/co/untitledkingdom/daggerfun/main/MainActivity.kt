package co.untitledkingdom.daggerfun.main

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import co.untitledkingdom.daggerfun.R
import co.untitledkingdom.daggerfun.detail.DetailActivity
import co.untitledkingdom.daggerfun.settings.SettingsActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.detailScreenButton
import kotlinx.android.synthetic.main.activity_main.settingsScreenButton
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        detailScreenButton.setOnClickListener { startActivity(Intent(this, DetailActivity::class.java)) }
        settingsScreenButton.setOnClickListener { startActivity(Intent(this, SettingsActivity::class.java)) }
        mainPresenter.bind()
    }
}
