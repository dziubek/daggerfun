package co.untitledkingdom.daggerfun.main

import co.untitledkingdom.daggerfun.GlobalService
import javax.inject.Inject

class MainService @Inject constructor(private val globalService: GlobalService)