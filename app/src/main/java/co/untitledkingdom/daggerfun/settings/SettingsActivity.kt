package co.untitledkingdom.daggerfun.settings

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import co.untitledkingdom.daggerfun.R
import co.untitledkingdom.daggerfun.settings.item.ItemFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_settings.itemFragmentButton
import javax.inject.Inject

class SettingsActivity : AppCompatActivity(), HasSupportFragmentInjector {


    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var settingsPresenter: SettingsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        itemFragmentButton.setOnClickListener {
            supportFragmentManager
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.fragmentContainer, ItemFragment())
                    .commit()
        }
        settingsPresenter.bind()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
}