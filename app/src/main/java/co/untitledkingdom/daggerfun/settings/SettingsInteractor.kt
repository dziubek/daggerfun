package co.untitledkingdom.daggerfun.settings

import javax.inject.Inject

class SettingsInteractor @Inject constructor(private val settingsService: SettingsService)