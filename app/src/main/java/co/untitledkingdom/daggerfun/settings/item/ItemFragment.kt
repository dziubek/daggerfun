package co.untitledkingdom.daggerfun.settings.item

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import co.untitledkingdom.daggerfun.R
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class ItemFragment : Fragment() {

    @Inject
    lateinit var itemPresenter: ItemPresenter

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        itemPresenter.bind()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item, container, false)
    }
}