package co.untitledkingdom.daggerfun.settings.item

import co.untitledkingdom.daggerfun.settings.SettingsInteractor
import javax.inject.Inject

class ItemInteractor @Inject constructor(private val settingsInteractor: SettingsInteractor)