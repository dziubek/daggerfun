package co.untitledkingdom.daggerfun.settings

import co.untitledkingdom.daggerfun.GlobalService
import javax.inject.Inject

class SettingsService @Inject constructor(private val globalService: GlobalService)